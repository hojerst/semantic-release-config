function triggerScript(name) {
    'use strict';
    return 'if [ -x .semantic-release/scripts/' + name + ' ] ; then .semantic-release/scripts/' + name + ' ; fi';
}

module.exports = {
    plugins: [
        [
            '@semantic-release/commit-analyzer',
            {
                preset: 'conventionalcommits'
            }
        ],
        [
            '@semantic-release/release-notes-generator',
            {
                preset: 'conventionalcommits'
            }
        ],
        [
            '@semantic-release/exec',
            {
                verifyConditionsCmd: triggerScript('verify-conditions'),
                analyzeCommitsCmd: triggerScript('analyze-commits'),
                verifyReleaseCmd: triggerScript('verify-release'),
                generateNotesCmd: triggerScript('generate-notes'),
                prepareCmd: triggerScript('prepare'),
                addChannelCmd: triggerScript('add-channel'),
                successCmd: triggerScript('success'),
                failCmd: triggerScript('fail'),
            }
        ],
        '@semantic-release/gitlab',
    ],
    branches: [
        'main',
        'master'
    ]
}
